import React, { Component } from 'react';
import Ticker from './ticker';
import LengthItem from './length-item';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      breakLength: 5,
      sessionLength: 25,
      timerLabel: "Session",
      secLeft: 1500,
      timeLeft: "25:00",
      active: false,
      rotDeg: 0
    };

    this._increment = this._increment.bind(this);
    this._decrement = this._decrement.bind(this);
    this._set_type = this._set_type.bind(this);
    this._countdown = this._countdown.bind(this);
    this._reset = this._reset.bind(this);
  }

  componentDidMount() {
    this.timer = null
  }

  _increment(item) {
    if (item === "break") {
      const timerLabel = this.state.timerLabel;
      const timeLeft = this.state.timeLeft;
      const newBreak = this.state.breakLength+1 > 60 ? 60 : this.state.breakLength+1;
      const newBreakLabel = newBreak < 10 ? `0${newBreak}:00` : `${newBreak}:00`;
      this.setState({
        breakLength: newBreak,
        secLeft: newBreak * 60,
        timeLeft: timerLabel === "Break" ? newBreakLabel : timeLeft
      });
    } else if (item === "session") {
      const timerLabel = this.state.timerLabel;
      const timeLeft = this.state.timeLeft;
      const newSession = this.state.sessionLength+1 > 60 ? 60 : this.state.sessionLength+1;
      const newSessionLabel = newSession < 10 ? `0${newSession}:00` : `${newSession}:00`;
      this.setState({
        sessionLength: newSession,
        secLeft: newSession * 60,
        timeLeft: timerLabel === "Session" ? newSessionLabel : timeLeft
      });
    }
  }

  _decrement(item) {
    if (item === "break") {
      const timerLabel = this.state.timerLabel;
      const timeLeft = this.state.timeLeft;
      const newBreak = this.state.breakLength-1 < 1 ? 1 : this.state.breakLength-1;
      const newBreakLabel = newBreak < 10 ? `0${newBreak}:00` : `${newBreak}:00`;
      this.setState({
        breakLength: newBreak,
        secLeft: newBreak * 60,
        timeLeft: timerLabel === "Break" ? newBreakLabel : timeLeft
      });
    } else if (item === "session") {
      const timerLabel = this.state.timerLabel;
      const timeLeft = this.state.timeLeft;
      const newSession = this.state.sessionLength-1 < 1 ? 1 : this.state.sessionLength-1;
      const newSessionLabel = newSession < 10 ? `0${newSession}:00` : `${newSession}:00`;
      this.setState({
        sessionLength: newSession,
        secLeft: newSession * 60,
        timeLeft: timerLabel === "Session" ? newSessionLabel : timeLeft
      });
    }
  }

  _set_type(type) {
    if (type === "break") {
      this.setState({
        timerLabel: "Break",
        timeLeft: `${this.state.breakLength}:00`
      });
    } else {
      this.setState({
        timerLabel: "Session",
        timeLeft: `${this.state.sessionLength}:00`
      });
    }
  }

  _countdown() {
    if (!this.state.active) {
      this.setState({
        active: true
      });

      // Assigns and starts interval for timer countdown
      this.timer = setInterval(() => {

        // Handle timer end
        if (this.state.secLeft == 0) {
          this.audioBeep.play();

          // Which timer are we on?
          const isBreak = this.state.timerLabel === "Session" ? false : true;

          // New label, prepends zero if less than 10, chooses break or session as appropriate
          const newLabel = isBreak ?
            (this.state.sessionLength < 10 ? `0${this.state.sessionLength}:00` : `${this.state.sessionLength}:00`) :
            (this.state.breakLength < 10 ? `0${this.state.breakLength}:00` : `${this.state.breakLength}:00`);

          // Update state
          this.setState({
            timerLabel: isBreak ? "Session" : "Break",
            secLeft: isBreak ? this.state.sessionLength * 60 : this.state.breakLength * 60,
            timeLeft: newLabel
          });

        } else {

          // Decrement seconds
          const newSec = this.state.secLeft - 1;
          const min = newSec / 60 < 10 ? `0${Math.floor(newSec / 60)}` : `${Math.floor(newSec / 60)}`;
          const sec = newSec % 60 < 10 ? `0${newSec % 60}` : `${newSec % 60}`;
          const typeLength = this.state.timerLabel === "Session" ? this.state.sessionLength : this.state.breakLength;
          // Update state
          this.setState({
            secLeft: newSec,
            timeLeft: `${min}:${sec}`,
            rotDeg: this.state.rotDeg + (360 / (typeLength * 60))
          });

        }
      }, 1000);

    } else {

      // If timer running pause
      clearInterval(this.timer);
      this.setState({
        active: false
      });

    }
  }

  _reset() {
    // Stop and reset audio
    this.audioBeep.pause();
    this.audioBeep.currentTime = 0;

    // Stop Timer
    clearInterval(this.timer);

    // Update state
    this.setState({
      breakLength: 5,
      sessionLength: 25,
      timerLabel: "Session",
      secLeft: 1500,
      timeLeft: "25:00",
      active: false,
      rotDeg: 0
    });
  }

  render() {
    const playBtn = <i class="fas fa-play"></i>;
    const pauseBtn = <i class="fas fa-pause"></i>;
    const tickerStyle = this.state.secLeft < 60 ? {color: "#B80212"} : null;
    const rot = `rotate(${this.state.rotDeg}deg)`;
    return (
      <div className='container'>
        <h1 id="title">Pomodoro Timer</h1>
        <LengthItem
          item="break"
          label="Break"
          length={this.state.breakLength}
          _increment={this._increment}
          _decrement={this._decrement}
          _set_type={this._set_type}
        />
        <img id="tomato" src="./style/assets/tomato_top.png" style={{transform: rot}}/>
        <LengthItem
         item="session"
         label="Session"
         length={this.state.sessionLength}
         _increment={this._increment}
         _decrement={this._decrement}
         _set_type={this._set_type}
        />
        <Ticker
          label={this.state.timerLabel}
          time={this.state.timeLeft}
          style={tickerStyle}
        />
        <button id="start_stop" onClick={this._countdown}>{this.state.active ? pauseBtn : playBtn}</button>
        <button id="reset" onClick={this._reset}><i class="fas fa-undo"></i></button>
        <audio id="beep" preload="auto"
          src="https://goo.gl/65cBl1"
          ref={(audio) => { this.audioBeep = audio; }} />
      </div>
    );
  }
}
