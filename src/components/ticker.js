import React from 'react';

const Ticker = (props) => {
  return (
    <div className="ticker-wrap">
      <h2 id="timer-label">{props.label}</h2>
      <div id="time-left" style={props.style}>{props.time}</div>
    </div>
  );
}

export default Ticker;