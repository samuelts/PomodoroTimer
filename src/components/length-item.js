import React, { Component } from 'react';

class LengthItem extends Component {
  constructor(props) {
    super(props);

    this._handleDecrement = this._handleDecrement.bind(this);
    this._handleIncrement = this._handleIncrement.bind(this);
    this._handleType = this._handleType.bind(this);
  }

  _handleDecrement() {
    this.props._decrement(this.props.item);
  }

  _handleIncrement() {
    this.props._increment(this.props.item);
  }

  _handleType() {
    this.props._set_type(this.props.item);
  }

  render() {
    return (
      <div className="length-item" id={this.props.item}>
        <h3
          id={`${this.props.item}-label`}
          onClick={this._handleType}
        >
          {this.props.label}
        </h3>
        <div>

          <button
            id={`${this.props.item}-increment`}
            onClick={this._handleIncrement}
          >
            <i class="fas fa-chevron-up"></i>
          </button>

          <div id={`${this.props.item}-length`}>{this.props.length}</div>

          <button
            id={`${this.props.item}-decrement`}
            onClick={this._handleDecrement}
          >
            <i class="fas fa-chevron-down"></i>
          </button>

        </div>
      </div>
    );
  }
}
export default LengthItem;