# PomodoroTimer

A Pomodoro Timer written with React

Demo here: https://samuelts.com/PomodoroTimer

## Images 
![Screenshot](https://raw.githubusercontent.com/safwyls/logos/master/PomodoroTimer.png)
